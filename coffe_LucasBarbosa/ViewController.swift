//
//  ViewController.swift
//  coffe_LucasBarbosa
//
//  Created by COTEMIG on 10/11/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Cafe: Decodable {
    let file: String
}

class ViewController: UIViewController {
        
    @IBOutlet weak var imageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        getNovaImagem()
    }
    
    @IBAction func reloadImg(_ sender: Any) {
        getNovaImagem()
    }
    
    func getNovaImagem() {
        AF.request("https://coffee.alexflipnote.dev/random.json").responseDecodable(of: Cafe.self) { response in
            if let cafe = response.value{
                self.imageView.kf.setImage(with: URL(string: cafe.file))
                
            }
        }
    }
}

